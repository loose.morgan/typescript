import { Moteur } from "../Moteur";
class MoteurEssence extends Moteur {
  constructor(vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
  }
}
