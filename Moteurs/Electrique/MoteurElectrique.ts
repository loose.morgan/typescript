import { Moteur } from "../Moteur";
class MoteurElectrique extends Moteur {
  constructor(vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
  }
}
