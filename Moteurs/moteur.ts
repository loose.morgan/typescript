export class Moteur {
  private _vitesse: number;
  private _kilometrage: number;

  constructor(vitesse: number, kilometrage: number) {
    this._vitesse = vitesse;
    this._kilometrage = kilometrage;
  }
  get vitesse(): number {
    return this._vitesse;
  }
  set vitesse(valeur: number) {
    this._vitesse = valeur;
  }
  get kilometrage(): number {
    return this._kilometrage;
  }
}
