import { Moteur } from "../Moteur";

class MoteurDiesel extends Moteur {
  constructor(vitesse: number, kilometrage: number) {
    super(vitesse, kilometrage);
  }
}
