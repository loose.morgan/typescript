import { Moteur } from "./Moteurs/Moteur";
export class Vehicule {
  private _marque: string;
  private _couleur: string;
  private _prix: number;
  private _moteur: Moteur;

  constructor(marque: string, couleur: string, prix: number, moteur: Moteur) {
    this._marque = marque;
    this._couleur = couleur;
    this._prix = prix;
    this._moteur = moteur;
  }

  set vitesse(vitesse: string) {
    this._marque = vitesse;
  }
  get vitesse(): string {
    return this.vitesse;
  }

  set couleur(couleur: string) {
    this._couleur = couleur;
  }
  get couleur(): string {
    return this._couleur;
  }

  set moteur(moteur: Moteur) {
    this._moteur = moteur;
  }
  get moteur(): Moteur {
    return this._moteur;
  }
  set prix(prix: number) {
    this._prix = prix;
  }
  get prix(): number {
    return this._prix;
  }
}
